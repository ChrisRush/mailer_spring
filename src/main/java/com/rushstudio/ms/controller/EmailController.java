package com.rushstudio.ms.controller;

import com.rushstudio.ms.model.Contact;
import com.rushstudio.ms.service.ContactService;
import com.rushstudio.ms.service.EmailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Produces;
import java.util.List;

@RestController
@RequestMapping(path = "/email")
public class EmailController {

    private ContactService contactService;
    private EmailService emailService;

    public EmailController(EmailService emailService, ContactService contactService) {
        this.emailService = emailService;
        this.contactService = contactService;
    }

    @GetMapping(path = "/sendTest")
    public String setTestMail() {
        emailService.sendTestMessage();
        return new String("done");
    }

    @GetMapping(path = "/sendFirst")
    @Produces(value = "application/json")
    public ResponseEntity<List<Contact>> sendFirst() {
        return new ResponseEntity<>(emailService.sendFirstEmail(), HttpStatus.OK);
    }

    @GetMapping(path = "/sendSecond")
    @Produces(value = "application/json")
    public ResponseEntity<List<Contact>> sendSecond() {
        return new ResponseEntity<>(emailService.sendSecondEmail(), HttpStatus.OK);
    }

    @GetMapping(path = "/sendThird")
    @Produces(value = "application/json")
    public ResponseEntity<List<Contact>> sendThird() {
        return new ResponseEntity<>(emailService.sendThirdEmail(), HttpStatus.OK);
    }
}
