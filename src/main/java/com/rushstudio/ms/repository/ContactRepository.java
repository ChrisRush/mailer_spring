package com.rushstudio.ms.repository;

import com.rushstudio.ms.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query("select c from Contact c where c.active = true and c.automationStep = :automationStep")
    List<Contact> findActiveByAutomationStep(Integer automationStep);
}
