package com.rushstudio.ms.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ScrapeObject {

    @CsvBindByName(column = "Company")
    private String company;
    @CsvBindByName(column = "Address 1")
    private String address;
    @CsvBindByName(column = "City")
    private String city;
    @CsvBindByName(column = "Region")
    private String region;
    @CsvBindByName(column = "ZIP")
    private String zip;
    @CsvBindByName(column = "Country")
    private String country;
    @CsvBindByName(column = "Phone")
    private String phone;
    @CsvBindByName(column = "Email")
    private String email;
    @CsvBindByName(column = "Website")
    private String website;
    @CsvBindByName(column = "Category")
    private String category;
}
